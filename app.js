const Milight = require('node-milight-promise').MilightController;
const commands = require('node-milight-promise').commandsV6;

const controllerIp = process.env.CONTROLLER_IP || '192.168.0.255';
const light = new Milight({
   ip: controllerIp,
   type: 'v6'
});

const express = require('express')
const app = express();

app.use(express.static('public'))

app.get('/:zone/:action/:arg?', (req, res) => {
   const { zone, action, arg } = req.params;
   controlLights(zone, action, arg);
   res.send(`[${new Date().toISOString()}] Zone: ${zone}, Action: ${action}, DimnessLevel: ${arg || '--'}`)
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Controller IP: ${controllerIp}.\nLight controller listening on ${port}`));

function controlLights(zone, action, dimnessLevel) {
   console.log(`[${new Date().toISOString()}] Zone: ${zone}, Action: ${action}, DimnessLevel: ${dimnessLevel || '--'}`)

   switch (action) {
      case 'on':
         light.sendCommands(
            commands.rgbw.on(zone),
            commands.rgbw.whiteMode(zone),
            commands.rgbw.brightness(zone, 100)
         );
         break;

      case 'off':
         light.sendCommands(commands.rgbw.off(zone));
         break;

      case 'dim':
      case 'bright':
      case 'brighten':
         light.sendCommands(commands.rgbw.brightness(zone, dimnessLevel));
         break;
   }
}
