if [ $1 ]
then
    echo "Starting container with version:" $1
    docker run -d --restart unless-stopped -p 1337:3000 -e CONTROLLER_IP=192.168.0.26 --name milight-web lukepammant/milight-web-controller:$1
else
    echo "ERROR: No version defined"
fi
