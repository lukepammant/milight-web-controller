if [ $1 ]
then
    echo "Building docker image with version:" $1
    docker build -t lukepammant/milight-web-controller:$1 . 
else
    echo "ERROR: No version defined"
fi