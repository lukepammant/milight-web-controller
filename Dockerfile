FROM node:8-slim

WORKDIR /server

COPY package.json package.json
RUN npm install

COPY . /server
RUN npm run build

EXPOSE 3000
CMD [ "npm", "start" ]